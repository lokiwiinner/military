$(document).ready(function () {
	function branch() {
		var branch = $('.branch-select__default'),
			select_hidden = $('.branch-select__items-hidden'),
			select_items = $('.branch-select__items'),
			select_items_height = select_items.height();
		branch.click(function () {
			if (branch.hasClass('active')) {
				branch.removeClass('active');
				select_hidden.css({'height': 0});
			} else {
				branch.addClass('active');
				select_hidden.css({'height': select_items_height});
			}
		})
	}

	branch();

	function date() {
		$('#date, #date_2').glDatePicker(
			{
				monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
				dowNames: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
				onClick: function (target, cell, date) {
					target.val(date.getDate() + '.' +
						(Number(date.getMonth() + 1) < 10 ? '0' + Number(date.getMonth() + 1) : Number(date.getMonth() + 1)) + '.' +
						date.getFullYear());
				},
			}
		);
	}

	if($(window).width() > 767){
		date();
	}else{
		$('.data').attr('type', 'tel')
	}


	function check_add() {
		$('.check-add').click(function () {
			if($(this).prop('checked')){
				$('.booking-items__check').slideUp('slow')
			}else{
				$('.booking-items__check').slide('slow')
			}
		})
	}
	check_add();

	function slick() {
		$('.index-slider').slick({
			prevArrow: '<div class="icon-left icon-arrow-point-to-right"></div>',
			nextArrow: '<div class="icon-right icon-arrow-point-to-right"></div>',
			dots: true,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						dots: false,
					}
				},
			]
		})
		$('.number-slider').slick({
			prevArrow: '<div class="icon-left icon-arrow-point-to-right"></div>',
			nextArrow: '<div class="icon-right icon-arrow-point-to-right"></div>',
			dots: true,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						arrows: false,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						dots: true,
						arrows: false
					}
				}
			]
		})
		$('.index-video').slick({
			prevArrow: '<div class="icon-left icon-arrow-point-to-right"></div>',
			nextArrow: '<div class="icon-right icon-arrow-point-to-right"></div>',
			dots: false
		})
		$('.index-program__items').slick({
			prevArrow: '<div class="icon-left icon-arrow-point-to-right"></div>',
			nextArrow: '<div class="icon-right icon-arrow-point-to-right"></div>',
			dots: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						dots: true,
						arrows: false
					}
				}
			]
		})
		$('.reviews-program').slick({
			arrows: false,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
		})
		$('.procedure-items, .recommendations-procedure__items').slick({
			prevArrow: '<div class="icon-left icon-arrow-point-to-right"></div>',
			nextArrow: '<div class="icon-right icon-arrow-point-to-right"></div>',
			dots: false,
			infinite: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						dots: true,
						arrows: false
					}
				}
			]
		})
		$('.photo-slider').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true,
			prevArrow: '<div class="icon-left icon-arrow-point-to-right"></div>',
			nextArrow: '<div class="icon-right icon-arrow-point-to-right"></div>',
		})
	}

	slick();

	function reviews_text() {
		var newText;
		var container = $('.index-reviews__content-container')

		var size = 375;
		var n = 370;
		container.each(function () {
			var content = $(this).find('.index-reviews__content');
			var text = content.innerHTML;
			var $this = $(this);
			if ($this.find('.index-reviews__content').html().length > size) {
				newText = $this.find('.index-reviews__content').html().substr(n)
				text = $this.find('.index-reviews__content').html().substr(0, n);
			}
			else {
				text = $this.find('.index-reviews__content').html();
			}
			$this.next('.index-reviews__flex').find('.index-reviews__link').on('click', function () {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).html('Читать полностью');
					$this.removeClass('active');
					$this.find('.index-reviews__content').html(text + '...');
				} else {
					$(this).addClass('active');
					$(this).html('Скрыть');
					$this.addClass('active');
					$this.find('.index-reviews__content').html(text);
				}
			})
			$this.find('.index-reviews__content').html(text + '...');
			$this.find('span').html(newText);
			console.log($this.find('span').html(newText))
		})
	}

	reviews_text();

	function header() {
		$(window).scroll(function () {
			var menu = $('header');
			if ($(this).scrollTop() > 300) {
				menu.addClass('active')
			} else {
				menu.removeClass('active')
			}
		})
		if ($(window).width() < 991) {
			$('.header-container').mmenu({
				navbar: {
					title: "Меню",
				}
			});
			$(".hamburger").click(function () {
				if ($(".hamburger").hasClass("active")) {
					$('.hamburger').removeClass('active')
				} else {
					$('.hamburger').addClass('active')
				}
			})
			$('.mm-page__blocker').on('click mousedown touchstart', function () {
				$('.hamburger').removeClass('active')
			});
		}
		$('.btn-booking__price').click(function (e) {
			e.preventDefault();
			$('.header-armor').addClass('active')
		})
	}

	header();

	$('.select-container').select2();
	$('a[data-fancybox="images"]').fancybox({
		loop: true
	})
	$('.phone').mask('+9(999) 999-99-99')
	$('.data').mask('99.99.9999')

	function map() {
		var myMap
		ymaps.ready(function () {
			if (!myMap) {
				myMap = new ymaps.Map('map', {
					center: [44.143528, 43.017175],
					zoom: 15
				}, {
					searchControlProvider: 'yandex#search'
				}),
					myPlacemark = new ymaps.Placemark([44.143528, 43.017175], {
						balloonContentHeader: "Адресс санатория",
						balloonContentFooter: "357401, Ставропольский край, г. Железноводск, ул. Чапаева, 9-15",
						hintContent: "г. Железноводск, ул. Чапаева, 9-15"
					});
				myMap.geoObjects.add(myPlacemark);
				myMap.behaviors.disable('scrollZoom')
			}
		})

	}

	map()

	function news_text() {
		console.log(1)
		var elem, size, text;
		var elem = $('.news-item');
		var size = 240;
		var n = 238;
		elem.each(function () {
			var $this = $(this),
				$this_find = $this.find('.news-item__description'),
				$this_find_text = $this_find.innerHTML;
			if ($this_find[0].innerHTML.length > size) {
				$this_find_text = $this_find[0].innerHTML.substr(0, n);
				$this_find[0].innerHTML = $this_find_text + '...';
			}
			else {
				$this_find_text = $this_find[0].innerHTML;
				$this_find[0].innerHTML = $this_find_text;
			}
		})
	}

	news_text();

	function table_price() {
		var hiddenMenuContainer = $('.category-container__table-hidden'),
			hiddenMenu = $('.category-container__table-height'),
			hiddenMenuHeight = hiddenMenu.height(),
			activeElementClick = $('.active-element'),
			activeElementId = $('#table-date');
		activeElementClick.on('click', function () {
			var $this = $(this);
			if ($this.hasClass('click')) {
				$this.removeClass('click');
				hiddenMenuContainer.css({'height': 0});
			} else {
				$this.addClass('click');
				hiddenMenuContainer.css({'height': hiddenMenuHeight});
			}
		});
		$(document).mouseup(function (e) {
			if (!hiddenMenuContainer.is(e.target) && hiddenMenuContainer.has(e.target).length === 0 && !activeElementClick.is(e.target) && activeElementClick.has(e.target).length === 0) {
				activeElementClick.removeClass('click');
				hiddenMenuContainer.css({'height': 0});
			}
		});
		hiddenMenuContainer.on('click', '.price-table__th', function () {
			var $this = $(this),
				dataAttr = $this.data('date');
			$('[data-date_price~="' + dataAttr + '"]').addClass('active').siblings().removeClass('active');
			activeElementId.empty();
			$this.find('span').clone().appendTo(activeElementId);
			$this.addClass('active').siblings().removeClass('active');
			activeElementClick.removeClass('click');
			hiddenMenuContainer.css({'height': 0});
		})
	}

	table_price();

	function blockHeight() {
		var height = 0;
		$('.news-item').each(function () {
			var $this = $(this),
				$this_height = $this.height();
			if ($this_height > height) {
				height = $this_height
			}
		})
		$('.news-item').css({'height': height})
	}

	blockHeight();

	function answer() {
		$('.answers-link').click(function () {
			if ($(this).hasClass('active')) {
				$(this).next('.answer-form').slideUp('slow');
				$(this).removeClass('active');
				$(this).html('Ответить');
			} else {
				$(this).next('.answer-form').slideDown('slow');
				$(this).addClass('active');
				$(this).html('скрыть форму');
			}
		})
	}

	answer();
})